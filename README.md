### Weather Cache

To run quickly, run:

```bash
docker run -e API_KEY=(api key from weatherapi.com) --tmpfs /var/lib/varnish:exec -p 8080:80 --rm -it $(docker build -q .)
```

Questions?  Open up an issue.
