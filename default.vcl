vcl 4.0;

import std;

backend default {
  .host = "api.weatherapi.com";
  .port = "80";
}

sub vcl_recv {
    # Happens before we check if we have this in cache already.
    #
    # Typically you clean up the request here, removing cookies you don't need,
    # rewriting the request, etc.

    set req.http.Host = "api.weatherapi.com";

    if (req.url ~ "/\?q=") {
	set req.http.X-Contact-Info = "me at ianwilson dot org";
	set req.url = regsuball(req.url, "/\?q=", "/v1/current.json?q=");
	set req.url = req.url + "&key=" + std.getenv("API_KEY");
    }

    if (req.method != "GET" &&
      req.method != "HEAD" &&
      req.method != "PUT" &&
      req.method != "POST" &&
      req.method != "TRACE" &&
      req.method != "OPTIONS" &&
      req.method != "DELETE") {
        /* while the visitor is using a non-valid HTTP method */
        return (synth(404, "Non-valid HTTP method!"));
    }

    if (req.method != "GET" && req.method != "HEAD") {
        /* We only deal with GET and HEAD by default */
      return (pass);
    }

    return (hash);
}

sub vcl_backend_response {
    # Happens after we have read the response headers from the backend.
    #
    # Here you clean the response headers, removing silly Set-Cookie headers
    # and other mistakes your backend does.

    if (bereq.method != "GET" && bereq.method != "HEAD" && bereq.method != "PURGE") {
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;
        return (deliver);
    }

    if (bereq.url ~ "/api/(rest|soap|v2_soap)") {
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;
        return (deliver);
    }
    elseif (bereq.url !~ "/api/.+") {
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;
        return (deliver);
    }
    if (beresp.status != 200) {
        # Do not cache non-200 responses
        set beresp.ttl = 120s;
        set beresp.uncacheable = true;
        return (deliver);
    } else {
        if (bereq.url ~ "^/api") {
            unset beresp.http.Set-Cookie;
            set beresp.ttl = 4h;  # cache for 4 hours
            set beresp.grace = 4h;
            return (deliver);
        }
    }

    return (deliver);


    # Don't cache 50x responses
    if (beresp.status == 500 || beresp.status == 502 || beresp.status == 503 || beresp.status == 504) {
        return (abandon);
    }

    set beresp.ttl = 10s;
    # If the backend fails, keep serving out of the cache for 30m
    set beresp.grace = 30m;
    return (deliver);
}

# Only handle actual PURGE HTTP methods, everything else is discarded
sub vcl_purge {
  if (req.method == "PURGE") {
    # restart request
    set req.http.X-Purge = "Yes";
    return (restart);
  }
}

sub vcl_deliver {
    # Happens when we have all the pieces we need, and are about to send the
    # response to the client.
    #
    # You can do accounting or modifying the final object here.

    if (obj.hits > 0) { # Add debug header to see if it's a HIT/MISS and the number of hits, disable when not needed
        set resp.http.X-Cache = "HIT";
    } else {
        set resp.http.X-Cache = "MISS";
    }
}


